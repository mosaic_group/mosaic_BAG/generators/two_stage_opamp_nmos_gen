from typing import *

from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackManager, TrackID

from sal.transistor import *
from sal.routing_grid import RoutingGridHelper

from .params import two_stage_opamp_nmos_layout_params


class layout(AnalogBase):
    """
    A Two Stage Operational Amplifier with NMOS input pair

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls):
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='two_stage_opamp_nmos_layout_params parameter object',
        )

    def draw_layout(self):
        params: two_stage_opamp_nmos_layout_params = self.params['params']

        # Define what layers will be used for horizontal and vertical connections
        # horiz_conn_layer = self.mos_conn_layer + 1
        vert_conn_layer = self.mos_conn_layer + 2

        ################################################################################
        # 0:   Floorplan the design
        #
        # Know where transistors go and where the horizontal
        # and vertical connections will be
        # Also determine how transistors will be aligned horizontally relative to eachother
        # and know how things will need to shift as various sizes scale
        ################################################################################

        ################################################################################
        # 1:   Set up track allocations for each row
        #
        # Use TrackManager to create allocate track and spacing based on the floorplan
        #   -  Rather than explicitly allocate a number of gate and source/drain tracks for each row (as is done in
        #      the bootcamp modules), instead define a list of what horizontal connects will be required for each row.
        #   -  Based on spacing and width rules provided in the specification file, BAG/trackmanager will calculate
        #      how many tracks each row needs
        # Finally, initialize the rows using the helper functions above
        ################################################################################
        tr_manager = TrackManager(grid=self.grid, tr_widths=params.tr_widths, tr_spaces=params.tr_spaces)

        # Rows are ordered from bottom to top
        # To use TrackManager, an ordered list of wiring types and their locations must be provided.
        # Define two lists, one for the nch rows and one for the pch rows
        # The lists are composed of dictionaries, one per row.
        # Each dictionary has two list entries (g and ds), which are ordered lists of what wire types will be present
        #  in the g and ds sections of that row. Ordering is from bottom to top of the design.

        # Set up the row information
        # Row information contains the row properties like width/number of fins, orientation, intent, etc.
        # Storing in a row dictionary/object allows for convenient fetching of data in later functions

        row_Dummy_NB = Row(name='Dummy_NB',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NB'],
                           threshold=params.th_dict['Dummy_NB'],
                           wire_names_g=['sig1', 'sig1'],
                           wire_names_ds=['sig1', 'sig1']
                           )
        row_tail_B = Row(name='tail_B',
                         orientation=RowOrientation.R0,
                         channel_type=ChannelType.N,
                         width=params.w_dict['tail_B'],
                         threshold=params.th_dict['tail_B'],
                         wire_names_g=['sig1', 'cmfb', 'bias'],
                         wire_names_ds=['sig', 'sig1']
                         )
        row_tail_T = Row(name='tail_T',
                         orientation=RowOrientation.R0,
                         channel_type=ChannelType.N,
                         width=params.w_dict['tail_T'],
                         threshold=params.th_dict['tail_T'],
                         wire_names_g=['sig1', 'sig1', 'bias'],
                         wire_names_ds=['sig', 'sig1']
                         )
        row_in_B = Row(name='In_B',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.N,
                       width=params.w_dict['In_B'],
                       threshold=params.th_dict['In_B'],
                       wire_names_g=['sig', 'sig', 'sig'],
                       wire_names_ds=['sig', 'sig', 'sig']
                       )
        row_in_T = Row(name='In_T',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.N,
                       width=params.w_dict['In_T'],
                       threshold=params.th_dict['In_T'],
                       wire_names_g=['sig', 'sig', 'sig'],
                       wire_names_ds=['sig', 'sig', 'sig1']
                       )
        row_Dummy_NT = Row(name='Dummy_NT',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NT'],
                           threshold=params.th_dict['Dummy_NT'],
                           wire_names_g=['sig1', 'sig1'],
                           wire_names_ds=['sig1', 'sig1']
                           )
        row_Dummy_PB = Row(name='Dummy_PB',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.P,
                           width=params.w_dict['Dummy_PB'],
                           threshold=params.th_dict['Dummy_PB'],
                           wire_names_g=['sig1', 'sig1'],
                           wire_names_ds=['sig1', 'sig1']
                           )
        row_load_B = Row(name='load_B',
                         orientation=RowOrientation.R0,
                         channel_type=ChannelType.P,
                         width=params.w_dict['load_B'],
                         threshold=params.th_dict['load_B'],
                         wire_names_g=['sig1', 'sig1', 'sig'],
                         wire_names_ds=['sig', 'outn', 'outp']
                         )
        row_load_T = Row(name='load_T',
                         orientation=RowOrientation.R0,
                         channel_type=ChannelType.P,
                         width=params.w_dict['load_T'],
                         threshold=params.th_dict['load_T'],
                         wire_names_g=['sig1', 'sig'],
                         wire_names_ds=['sig', 'sig1', 'sig1']
                         )
        row_Dummy_PT = Row(name='Dummy_PT',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.P,
                           width=params.w_dict['Dummy_PT'],
                           threshold=params.th_dict['Dummy_PT'],
                           wire_names_g=['sig1', 'sig1'],
                           wire_names_ds=['sig1', 'sig1']
                           )

        # Define the order of the rows (bottom to top) for this analogBase cell
        rows = RowList(
            [row_Dummy_NB, row_tail_B, row_tail_T, row_in_B, row_in_T, row_Dummy_NT,
             row_Dummy_PB, row_load_B, row_load_T, row_Dummy_PT]
        )

        ################################################################################
        # 2:
        # Initialize the transistors in the design
        # Storing each transistor's information (name, location, row, size, etc) in a dictionary object allows for
        # convenient use later in the code, and also greatly simplifies the schematic generation
        # The initialization sets the transistor's row, width, and source/drain net names for proper dummy creation
        ################################################################################

        fg_tail1 = params.seg_dict['tail1'] // 2
        fg_tail2 = params.seg_dict['tail2']
        fg_tailcm = params.seg_dict['tailcm']
        fg_in = params.seg_dict['in'] // 2
        fg_diode1 = params.seg_dict['diode1'] // 2
        fg_diode2 = params.seg_dict['diode2'] // 2
        fg_ngm1 = params.seg_dict['ngm1'] // 2
        fg_comp = params.seg_dict['comp'] // 2
        fg_vbt = params.seg_dict['vbt']
        fg_vb2t = params.seg_dict['vb2t']
        fg_vbb = params.seg_dict['vbb']
        fg_vb2b = params.seg_dict['vb2b']
        fg_vbm = params.seg_dict['vbm']
        fg_vb2m = params.seg_dict['vb2m'] // 2
        fg_res = params.seg_dict['res']
        fg_ref = params.seg_dict['ref']
        fg_cmfb = params.seg_dict['cmfb']
        fg_en = params.seg_dict['en']
        fg_invn = params.seg_dict['Invn']
        fg_invp = params.seg_dict['Invp']

        tail_1 = Transistor(name='tail_1', row=row_tail_B, fg=fg_tail1, deff_net='tail')
        tail_2 = Transistor(name='tail_2', row=row_tail_T, fg=fg_tail1, deff_net='tail')
        in_p_1 = Transistor(name='in_p_1', row=row_in_B, fg=fg_in, seff_net='tail', deff_net='midn')
        in_n_1 = Transistor(name='in_n_1', row=row_in_B, fg=fg_in, seff_net='tail', deff_net='midp')
        in_p_2 = Transistor(name='in_p_2', row=row_in_T, fg=fg_in, seff_net='tail', deff_net='midn')
        in_n_2 = Transistor(name='in_n_2', row=row_in_T, fg=fg_in, seff_net='tail', deff_net='midp')

        diod_L_1 = Transistor(name='diod_L_1', row=row_load_B, fg=fg_diode1, deff_net='midp')
        diod_R_1 = Transistor(name='diod_R_1', row=row_load_B, fg=fg_diode1, deff_net='midn')
        ngm_L_1 = Transistor(name='ngm_L_1', row=row_load_B, fg=fg_ngm1, deff_net='midp')
        ngm_R_1 = Transistor(name='ngm_R_1', row=row_load_B, fg=fg_ngm1, deff_net='midn')
        diod_L_2 = Transistor(name='diod_L_2', row=row_load_T, fg=fg_diode1, deff_net='midp')
        diod_R_2 = Transistor(name='diode_R_2', row=row_load_T, fg=fg_diode1, deff_net='midn')
        ngm_L_2 = Transistor(name='ngm_L_2', row=row_load_T, fg=fg_ngm1, deff_net='midp')
        ngm_R_2 = Transistor(name='ngm_R_2', row=row_load_T, fg=fg_ngm1, deff_net='midn')

        gain_L_1 = Transistor(name='gain_L_1', row=row_load_B, fg=fg_diode2, deff_net='outp')
        gain_R_1 = Transistor(name='gain_R_1', row=row_load_B, fg=fg_diode2, deff_net='outn')
        gain_L_2 = Transistor(name='gain_L_2', row=row_load_T, fg=fg_diode2, deff_net='outp')
        gain_R_2 = Transistor(name='gain_R_2', row=row_load_T, fg=fg_diode2, deff_net='outn')
        tail_L_2 = Transistor(name='tail_L_2', row=row_tail_T, fg=fg_tail2, deff_net='outp')
        tail_R_2 = Transistor(name='tail_R_2', row=row_tail_T, fg=fg_tail2, deff_net='outn')
        cmr_L = Transistor(name='cmr_L', row=row_tail_B, fg=fg_tailcm, deff_net='outp')
        cmr_R = Transistor(name='cmr_R', row=row_tail_B, fg=fg_tailcm, deff_net='outn')
        comp_L_1 = Transistor(name='comp_L_1', row=row_in_B, fg=fg_comp, seff_net='midp', deff_net='xn')
        comp_R_1 = Transistor(name='comp_R_1', row=row_in_B, fg=fg_comp, seff_net='midn', deff_net='xp')
        comp_L_2 = Transistor(name='comp_L_2', row=row_in_T, fg=fg_comp, seff_net='midp', deff_net='xn')
        comp_R_2 = Transistor(name='comp_R_2', row=row_in_T, fg=fg_comp, seff_net='midn', deff_net='xp')

        VB2T_L = Transistor(name='VB2T_L', row=row_load_B, fg=fg_vb2t, deff_net='vxn')
        VB2T_R = Transistor(name='VB2T_R', row=row_load_B, fg=fg_vb2t, deff_net='vxp')
        VB2B_L = Transistor(name='VB2B_L', row=row_tail_T, fg=fg_vb2b, deff_net='vb2tn')
        VB2B_R = Transistor(name='VB2B_R', row=row_tail_T, fg=fg_vb2b, deff_net='vb2tp')
        VB2M_L_1 = Transistor(name='VB2M_L_1', row=row_in_B, fg=fg_vb2m, deff_net='vxn', seff_net='vb2tn')
        VB2M_R_1 = Transistor(name='VB2M_R_1', row=row_in_B, fg=fg_vb2m, deff_net='vxp', seff_net='vb2tp')
        VB2M_L_2 = Transistor(name='VB2M_L_2', row=row_in_T, fg=fg_vb2m, deff_net='vxn', seff_net='vb2tn')
        VB2M_R_2 = Transistor(name='VB2M_R_2', row=row_in_T, fg=fg_vb2m, deff_net='vxp', seff_net='vb2tp')

        VBT = Transistor(name='VBT', row=row_load_B, fg=fg_vbt, deff_net='vbm')
        VBM = Transistor(name='VBM', row=row_in_B, fg=fg_vbm, deff_net='vbm', seff_net='vbb')
        VBB = Transistor(name='VBB', row=row_tail_T, fg=fg_vbb, deff_net='vbb')
        XRes = Transistor(name='XRes', row=row_in_B, fg=fg_res, deff_net='bias', seff_net='ref')
        XRef = Transistor(name='XRef', row=row_tail_T, fg=fg_ref, deff_net='ref')
        CMFB = Transistor(name='CMFB', row=row_load_B, fg=fg_cmfb, deff_net='cmfb_ibias')

        ENL = Transistor(name='ENL', row=row_tail_B, fg=fg_en, deff_net='bias')
        ENR = Transistor(name='ENR', row=row_tail_B, fg=fg_en, deff_net='bias')

        # INV as Buffer for Enable transistor
        Invn = Transistor(name='Invn', row=row_tail_B, fg=fg_invn, deff_net='bias')
        Invp = Transistor(name='Invp', row=row_load_B, fg=fg_invp, deff_net='bias')

        # Compose a list of all the transistors so it can be iterated over later
        transistors = [
            tail_1, tail_2, in_p_1, in_p_2, in_n_1, in_n_2, diod_L_1, diod_L_2, diod_R_1, diod_R_2,
            ngm_L_1, ngm_L_2, ngm_R_1, ngm_R_2,
            tail_L_2, tail_R_2, comp_L_1, comp_L_2, comp_R_1, comp_R_2,
            gain_L_1, gain_L_2, gain_R_1, gain_R_2, cmr_L, cmr_R,
            VB2B_L, VB2B_R, VB2M_L_1, VB2M_L_2, VB2M_R_1, VB2M_R_2, VB2T_L, VB2T_R,
            VBB, VBM, VBT, XRes, XRef, CMFB, ENR, ENL, Invn, Invp,
        ]

        ################################################################################
        # 3:   Calculate transistor locations
        # Based on the floorplan, want the tail, input, nmos regen, pmos regen, and pmos tail to be in a column
        # and for convenience, place the reset and load in a column, but right/left justified
        # Notation:
        #     fg_xxx refers to how wide (in fingers) a transistor or column of transistors is
        #     col_xxx refers to the location of the left most finger of a transistor or a column of transistors
        ################################################################################

        fg_stack = max(tail_1.fg // 2, in_p_1.fg, diod_R_1.fg + ngm_R_1.fg)
        # To reserve the space needed for vertical lines in case the transistors are too small
        if fg_stack % 2 == 1:
            fg_stack = fg_stack + 1
        if fg_stack < 6:
            fg_stack = 6

        fg_side = 0
        fg_side1 = max(VBB.fg, VBM.fg, VBT.fg)
        fg_side2 = max(CMFB.fg, XRes.fg, XRef.fg)
        fg_st2 = max(tail_R_2.fg, gain_R_1.fg, comp_R_1.fg, CMFB.fg, cmr_R.fg)
        fg_st3 = max(VB2T_R.fg, VB2M_R_2.fg, VB2B_R.fg, ENR.fg)
        fg_side_R = max(Invn.fg, Invp.fg) + 2

        # Add an explicit gap in the middle for symmetry. Set to 0 to not have gap
        fg_mid = 0

        # Get the minimum gap between fingers of different transistors
        # This varies between processes, so avoid hard-coding by using the method in self._tech_cls
        fg_space = 2

        fg_total = params.fg_dum + fg_side1 + fg_space + fg_side2 + fg_space + fg_st3 + fg_space + fg_st2 + fg_space + \
                   fg_stack + fg_mid + fg_stack + fg_space + fg_st2 + fg_space + fg_st3 + \
                   fg_space + fg_side_R + fg_space + fg_side + params.fg_dum

        # Calculate the starting column index for each stack of transistors
        col_side1_left = params.fg_dum
        col_side2_left = params.fg_dum + fg_side1 + fg_space
        col_st3_left = col_side2_left + fg_side2 + fg_space
        col_st2_left = col_st3_left + fg_st3 + fg_space
        col_stack_left = col_st2_left + fg_st2 + fg_space
        col_stack_right = col_stack_left + fg_stack + fg_mid
        col_st2_right = col_stack_right + fg_stack + fg_space
        col_st3_right = col_st2_right + fg_st2 + fg_space
        col_side_right = col_st3_right + fg_st3 + fg_space

        # Calculate positions of transistors
        # This uses helper functions to place each transistor within a stack/column of a specified starting index and
        # width, and with a certain alignment (left, right, centered) within that column
        tail_1.assign_column(offset=col_stack_left, fg_col=2 * fg_stack, align=TransistorAlignment.CENTER)
        tail_2.assign_column(offset=col_stack_left, fg_col=2 * fg_stack, align=TransistorAlignment.CENTER)
        in_n_1.assign_column(offset=col_stack_left, fg_col=fg_stack, align=TransistorAlignment.RIGHT)
        in_p_2.assign_column(offset=col_stack_left, fg_col=fg_stack, align=TransistorAlignment.RIGHT)
        diod_L_1.assign_column(offset=col_stack_left, fg_col=fg_stack // 2, align=TransistorAlignment.RIGHT)
        ngm_L_1.assign_column(offset=col_stack_left + fg_stack // 2, fg_col=fg_stack // 2,
                              align=TransistorAlignment.LEFT)
        diod_R_2.assign_column(offset=col_stack_left, fg_col=fg_stack // 2, align=TransistorAlignment.RIGHT)
        ngm_R_2.assign_column(offset=col_stack_left + fg_stack // 2, fg_col=fg_stack // 2,
                              align=TransistorAlignment.LEFT)

        in_p_1.assign_column(offset=col_stack_right, fg_col=fg_stack, align=TransistorAlignment.LEFT)
        in_n_2.assign_column(offset=col_stack_right, fg_col=fg_stack, align=TransistorAlignment.LEFT)
        ngm_R_1.assign_column(offset=col_stack_right, fg_col=fg_stack // 2, align=TransistorAlignment.RIGHT)
        diod_R_1.assign_column(offset=col_stack_right + fg_stack // 2, fg_col=fg_stack // 2,
                               align=TransistorAlignment.LEFT)
        ngm_L_2.assign_column(offset=col_stack_right, fg_col=fg_stack // 2, align=TransistorAlignment.RIGHT)
        diod_L_2.assign_column(offset=col_stack_right + fg_stack // 2, fg_col=fg_stack // 2,
                               align=TransistorAlignment.LEFT)

        tail_L_2.assign_column(offset=col_st2_left, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        cmr_L.assign_column(offset=col_st2_left, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        gain_L_1.assign_column(offset=col_st2_left, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        gain_L_2.assign_column(offset=col_st2_left, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        comp_L_1.assign_column(offset=col_st2_left, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        comp_L_2.assign_column(offset=col_st2_left, fg_col=fg_st2, align=TransistorAlignment.CENTER)

        tail_R_2.assign_column(offset=col_st2_right, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        cmr_R.assign_column(offset=col_st2_right, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        gain_R_1.assign_column(offset=col_st2_right, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        gain_R_2.assign_column(offset=col_st2_right, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        comp_R_1.assign_column(offset=col_st2_right, fg_col=fg_st2, align=TransistorAlignment.CENTER)
        comp_R_2.assign_column(offset=col_st2_right, fg_col=fg_st2, align=TransistorAlignment.CENTER)

        ENL.assign_column(offset=col_st3_left, fg_col=fg_st3, align=TransistorAlignment.CENTER)
        VB2B_L.assign_column(offset=col_st3_left, fg_col=fg_st3, align=TransistorAlignment.CENTER)
        VB2T_L.assign_column(offset=col_st3_left, fg_col=fg_st3, align=TransistorAlignment.CENTER)
        VB2M_L_1.assign_column(offset=col_st3_left, fg_col=fg_st3, align=TransistorAlignment.CENTER)
        VB2M_L_2.assign_column(offset=col_st3_left, fg_col=fg_st3, align=TransistorAlignment.CENTER)

        ENR.assign_column(offset=col_st3_right, fg_col=fg_st3, align=TransistorAlignment.CENTER)
        VB2B_R.assign_column(offset=col_st3_right, fg_col=fg_st3, align=TransistorAlignment.CENTER)
        VB2T_R.assign_column(offset=col_st3_right, fg_col=fg_st3, align=TransistorAlignment.CENTER)
        VB2M_R_1.assign_column(offset=col_st3_right, fg_col=fg_st3, align=TransistorAlignment.CENTER)
        VB2M_R_2.assign_column(offset=col_st3_right, fg_col=fg_st3, align=TransistorAlignment.CENTER)

        VBB.assign_column(offset=col_side2_left, fg_col=fg_side2, align=TransistorAlignment.CENTER)
        VBM.assign_column(offset=col_side2_left, fg_col=fg_side2, align=TransistorAlignment.CENTER)
        VBT.assign_column(offset=col_side2_left, fg_col=fg_side2, align=TransistorAlignment.CENTER)

        XRef.assign_column(offset=col_side1_left, fg_col=fg_side1, align=TransistorAlignment.CENTER)
        XRes.assign_column(offset=col_side1_left, fg_col=fg_side1, align=TransistorAlignment.CENTER)
        CMFB.assign_column(offset=col_side1_left, fg_col=fg_side1, align=TransistorAlignment.CENTER)

        Invn.assign_column(offset=col_side_right, fg_col=fg_side_R, align=TransistorAlignment.CENTER)
        Invp.assign_column(offset=col_side_right, fg_col=fg_side_R, align=TransistorAlignment.CENTER)

        ################################################################################
        # 4:  Assign the transistor directions (s/d up vs down)
        #
        # Specify the directions that connections to the source and connections to the drain will go (up vs down)
        # Doing so will also determine how the gate is aligned (ie will it be aligned to the source or drain)
        # See the bootcamp for more details
        # The helper functions used here help to abstract away whether the intended source/drain diffusion region of
        # a transistor occurs on the even or odd columns of that device (BAG always considers the even columns of a
        # device to be the 's').
        # These helper functions allow a user to specify whether the even columns should be the transistors effective
        #  source or effective drain, so that the user does not need to worry about BAG's notation.
        ################################################################################

        # Set tail transistor to have source on the leftmost diffusion (arbitrary) and source going down
        in_p_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        in_n_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        in_p_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        in_n_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)

        tail_1.set_matched_direction(reference=in_p_1, seff_dir=TransistorDirection.DOWN, aligned=False)
        tail_2.set_matched_direction(reference=tail_1, seff_dir=TransistorDirection.DOWN, aligned=True)

        diod_L_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        diod_L_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        diod_R_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        diod_R_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        ngm_L_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        ngm_R_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        if ngm_R_2.fg % 2 == 0:
            ngm_R_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
            ngm_L_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        else:
            ngm_R_1.set_directions(seff=EffectiveSource.D, seff_dir=TransistorDirection.DOWN)
            ngm_L_2.set_directions(seff=EffectiveSource.D, seff_dir=TransistorDirection.DOWN)

        tail_L_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        cmr_L.set_matched_direction(reference=tail_L_2, seff_dir=TransistorDirection.DOWN, aligned=True)

        gain_L_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        gain_L_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        comp_L_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        comp_L_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)

        tail_R_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        cmr_R.set_matched_direction(reference=tail_R_2, seff_dir=TransistorDirection.DOWN, aligned=True)

        gain_R_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        gain_R_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        comp_R_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        comp_R_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)

        VB2M_L_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        VB2M_L_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)

        VB2T_L.set_matched_direction(reference=VB2M_L_1, seff_dir=TransistorDirection.UP, aligned=True)
        VB2B_L.set_matched_direction(reference=VB2M_L_1, seff_dir=TransistorDirection.DOWN, aligned=False)
        ENL.set_matched_direction(reference=VB2B_L, seff_dir=TransistorDirection.DOWN, aligned=True)

        VB2M_R_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        VB2M_R_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)

        VB2T_R.set_matched_direction(reference=VB2M_R_1, seff_dir=TransistorDirection.UP, aligned=True)
        VB2B_R.set_matched_direction(reference=VB2M_R_1, seff_dir=TransistorDirection.DOWN, aligned=False)
        ENR.set_matched_direction(reference=VB2B_R, seff_dir=TransistorDirection.DOWN, aligned=True)

        VBB.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        VBM.set_matched_direction(reference=VBB, seff_dir=TransistorDirection.DOWN, aligned=False)
        VBT.set_matched_direction(reference=VBB, seff_dir=TransistorDirection.UP, aligned=False)

        XRef.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        XRes.set_matched_direction(reference=XRef, seff_dir=TransistorDirection.DOWN, aligned=False)
        CMFB.set_matched_direction(reference=XRef, seff_dir=TransistorDirection.UP, aligned=False)

        Invn.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        Invp.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)

        ################################################################################
        # 5:  Draw the transistor rows, and the transistors
        #
        # All the difficult setup has been complete. Drawing the transistors is simple now.
        # Note that we pass the wire_names dictionary defined above so that BAG knows how to space out
        # the transistor rows. BAG uses this to calculate how many tracks to allocate to each
        ################################################################################

        n_rows = rows.n_rows
        p_rows = rows.p_rows

        # Draw the transistor row bases
        self.draw_base(params.lch, fg_total, params.ptap_w, params.ntap_w,
                       n_rows.attribute_values('width'), n_rows.attribute_values('threshold'),
                       p_rows.attribute_values('width'), p_rows.attribute_values('threshold'),
                       tr_manager=tr_manager, wire_names=rows.wire_names_dict(),
                       n_orientations=n_rows.attribute_values('orientation'),
                       p_orientations=p_rows.attribute_values('orientation'),
                       top_layer=params.top_layer,
                       half_blk_x=True, half_blk_y=True,
                       guard_ring_nf=params.guard_ring_nf,
                       )

        # Draw the transistors
        for tx in transistors:
            ports = self.draw_mos_conn(mos_type=tx.row.channel_type.value,
                                       row_idx=rows.index_of_same_channel_type(tx.row),
                                       col_idx=tx.col,
                                       fg=tx.fg,
                                       sdir=tx.s_dir.value,
                                       ddir=tx.d_dir.value,
                                       s_net=tx.s_net,
                                       d_net=tx.d_net,
                                       gate_ext_mode=1,
                                       g_via_row=2,
                                       )
            tx.set_ports(g=ports['g'],
                         d=ports[tx.deff.value],
                         s=ports[tx.seff.value])

        ################################################################################
        # 6:  Define horizontal tracks on which connections will be made
        #
        # Based on the wire_names dictionary defined in step 1), create TrackIDs on which horizontal connections will
        #  be made
        ################################################################################

        row_tail_B_index = rows.index_of_same_channel_type(row_tail_B)
        row_tail_T_index = rows.index_of_same_channel_type(row_tail_T)
        row_in_B_index = rows.index_of_same_channel_type(row_in_B)
        row_in_T_index = rows.index_of_same_channel_type(row_in_T)
        row_load_B_index = rows.index_of_same_channel_type(row_load_B)
        row_load_T_index = rows.index_of_same_channel_type(row_load_T)

        tid_tail_cm_G = self.get_wire_id('nch', row_tail_B_index, 'g', wire_name='cmfb')
        tid_tail_B_EN = self.get_wire_id('nch', row_tail_B_index, 'g', wire_name='sig1')
        tid_tail_B_G = self.get_wire_id('nch', row_tail_B_index, 'g', wire_name='bias')
        tid_tail_T_G = self.get_wire_id('nch', row_tail_T_index, 'g', wire_name='bias')
        tid_tail_B_DS = self.get_wire_id('nch', row_tail_B_index, 'ds', wire_name='sig')
        tid_tail_T_DS = self.get_wire_id('nch', row_tail_T_index, 'ds', wire_name='sig')
        tid_in_B_G0 = self.get_wire_id('nch', row_in_B_index, 'g', wire_name='sig', wire_idx=0)
        tid_in_B_G1 = self.get_wire_id('nch', row_in_B_index, 'g', wire_name='sig', wire_idx=1)
        tid_in_B_G2 = self.get_wire_id('nch', row_in_B_index, 'g', wire_name='sig', wire_idx=2)
        tid_in_B_DS0 = self.get_wire_id('nch', row_in_B_index, 'ds', wire_name='sig', wire_idx=1)
        tid_in_B_DS1 = self.get_wire_id('nch', row_in_B_index, 'ds', wire_name='sig', wire_idx=2)
        tid_in_B_DS2 = self.get_wire_id('nch', row_in_B_index, 'ds', wire_name='sig', wire_idx=0)
        tid_in_T_G0 = self.get_wire_id('nch', row_in_T_index, 'g', wire_name='sig', wire_idx=0)
        tid_in_T_G1 = self.get_wire_id('nch', row_in_T_index, 'g', wire_name='sig', wire_idx=1)
        tid_in_T_DS0 = self.get_wire_id('nch', row_in_T_index, 'ds', wire_name='sig', wire_idx=0)
        tid_in_T_DS1 = self.get_wire_id('nch', row_in_T_index, 'ds', wire_name='sig', wire_idx=1)
        tid_load_B_G = self.get_wire_id('pch', row_load_B_index, 'g', wire_name='sig')
        tid_load_B_DS = self.get_wire_id('pch', row_load_B_index, 'ds', wire_name='sig')
        tid_load_B_DSP = self.get_wire_id('pch', row_load_B_index, 'ds', wire_name='outp')
        tid_load_B_DSN = self.get_wire_id('pch', row_load_B_index, 'ds', wire_name='outn')
        tid_load_T_G = self.get_wire_id('pch', row_load_T_index, 'g', wire_name='sig')
        tid_load_T_DS = self.get_wire_id('pch', row_load_T_index, 'ds', wire_name='sig')

        # ################################################################################
        # # 7:  Perform wiring
        # #
        # # Use the self.connect_to_tracks, self.connect_differential_tracks, self.connect_wires, etc
        # #  to perform connections
        # # Note that the drain/source/gate wire arrays of the transistors can be easily accessed as keys in the tx
        # # dictionary structure.
        # #
        # # Best practice:
        # #  - Avoid hard-coding widths and pitches
        # #    Instead, use TrackManger's get_width, get_space, or get_next_track functionality to make the design
        # #    fully portable across process
        # #  - Avoid hard-coding which layers connections will be on.
        # #    Instead use layers relative to self.mos_conn_layer
        # ################################################################################

        # Define vertical tracks for the connections, based on the location of the col_stack_left/right
        # Two drain1 connection on the left side and two on the right side, same goes for drain2 connections
        # connection of Drain Pins of diode connected and negative-gm transistors

        grid = RoutingGridHelper(
            template_base=self,
            unit_mode=True,
            layer_id=vert_conn_layer,
            track_width=tr_manager.get_width(vert_conn_layer, 'sig'),
        )

        tid_d1_L1_vert = grid.track_by_col(col_idx=col_stack_left, offset=1)
        tid_d2_L1_vert = grid.track_by_col(col_idx=col_stack_left + fg_stack // 2 - 2)
        tid_d1_L2_vert = grid.track_by_col(col_idx=col_stack_left + fg_stack // 2 + 1)
        tid_d2_L2_vert = grid.track_by_col(col_idx=col_stack_left + fg_stack - 2)
        tid_d1_R1_vert = grid.track_by_col(col_idx=col_stack_right + 1)
        tid_d2_R1_vert = grid.track_by_col(col_idx=col_stack_right + fg_stack // 2 - 2)
        tid_d1_R2_vert = grid.track_by_col(col_idx=col_stack_right + fg_stack // 2 + 1)
        tid_d2_R2_vert = grid.track_by_col(col_idx=col_stack_right + fg_stack, offset=-1)

        # Connection of Gate Pins of diode connected and negative-gm transistors
        tid_G1_L_vert = grid.track_by_col(col_idx=col_stack_left, offset=-1)
        tid_G2_L_vert = grid.track_by_col(col_idx=col_stack_left)
        tid_G1_R_vert = grid.track_by_col(col_idx=col_stack_right + fg_stack, offset=1)
        tid_G2_R_vert = grid.track_by_col(col_idx=col_stack_right + fg_stack)

        warr_d_tail1 = self.connect_to_tracks([tail_1.d], tid_tail_B_DS, min_len_mode=0)
        warr_d_tail2 = self.connect_to_tracks([tail_2.d], tid_tail_T_DS, min_len_mode=0)
        warr_s_in = self.connect_to_tracks([in_p_1.s, in_n_1.s, in_p_2.s, in_n_2.s], tid_in_B_DS2, min_len_mode=0)

        vm_layer = 4
        pitch_vm = self.grid.get_track_pitch(vm_layer + 1, unit_mode=True)
        num = (max(warr_d_tail1.upper_unit, warr_d_tail2.upper_unit) -
               min(warr_d_tail1.lower_unit, warr_d_tail2.lower_unit)) // (4 * pitch_vm)
        if num == 0:
            num = 1

        tid_G_tail_vert = TrackID(
            layer_id=vert_conn_layer,
            track_idx=self.grid.coord_to_nearest_track(
                layer_id=vert_conn_layer,
                coord=max(warr_d_tail1.lower_unit, warr_d_tail2.lower_unit),
                half_track=False,
                mode=1,
                unit_mode=True
            ),
            width=tr_manager.get_width(vert_conn_layer, 'sig'),
            num=num,
            pitch=4,
        )

        tid_d_tail_vert = TrackID(
            layer_id=vert_conn_layer,
            track_idx=self.grid.coord_to_nearest_track(
                layer_id=vert_conn_layer,
                coord=(max(warr_d_tail1.lower_unit, warr_d_tail2.lower_unit)) + 2 * pitch_vm,
                half_track=False,
                mode=1,
                unit_mode=True
            ),
            width=tr_manager.get_width(vert_conn_layer, 'sig'),
            num=num,
            pitch=4,
        )

        tid_d_st2_L_vert = grid.track_by_col(col_idx=col_st2_left + fg_st2 // 2, half_track=True)
        tid_d_st3_L_vert = grid.track_by_col(col_idx=col_st3_left)
        tid_d_st2_R_vert = grid.track_by_col(col_idx=col_st2_right + fg_st2 // 2, half_track=True)

        warr_d_comp_R_1 = self.connect_to_tracks(
            [comp_R_1.d],
            tid_in_B_DS0,
            min_len_mode=0,
        )
        tid_d_st2_R1_vert = TrackID(
            layer_id=vert_conn_layer,
            track_idx=self.grid.coord_to_nearest_track(vert_conn_layer, warr_d_comp_R_1.upper_unit, unit_mode=True),
            # changed by F
            width=tr_manager.get_width(vert_conn_layer, 'sig'),
        )
        warr_d_comp_L_1 = self.connect_to_tracks(
            [comp_L_1.d],
            tid_in_B_DS0,
            min_len_mode=0,
        )
        tid_d_st2_L1_vert = TrackID(
            layer_id=vert_conn_layer,
            track_idx=self.grid.coord_to_nearest_track(vert_conn_layer, warr_d_comp_L_1.lower_unit, unit_mode=True,
                                                       half_track=True),  # chaged by F
            width=tr_manager.get_width(vert_conn_layer, 'sig'),
        )

        tid_d_st3_R_vert = grid.track_by_col(col_idx=col_st3_right)
        tid_d_sid2_vert = grid.track_by_col(col_idx=col_side2_left + fg_side2 // 2)
        tid_d_side1_vert = grid.track_by_col(col_idx=col_side1_left + fg_side1 // 2)
        tid_D_sideR_vert = grid.track_by_col(col_idx=col_side_right)
        tid_G_sideR_vert = grid.track_by_col(col_idx=col_side_right + fg_side_R)

        # Connect horizontal tracks using Vertical track-ids
        warr_g_invn = self.connect_to_tracks(
            [Invn.g],
            tid_tail_B_EN,
            min_len_mode=1
        )
        warr_g_invp = self.connect_to_tracks(
            [Invp.g],
            tid_load_B_G,
            min_len_mode=1
        )
        warr_d_invn = self.connect_to_tracks(
            [Invn.d],
            tid_tail_B_DS,
            min_len_mode=1
        )
        warr_d_invp = self.connect_to_tracks(
            [Invp.d],
            tid_load_B_DS,
            min_len_mode=1
        )
        warr_d_in_p1 = self.connect_to_tracks(
            [in_p_1.d],
            tid_in_B_DS0,
            min_len_mode=1
        )
        warr_d_in_n1 = self.connect_to_tracks(
            [in_n_1.d],
            tid_in_B_DS1,
            min_len_mode=1
        )
        warr_d_in_p2 = self.connect_to_tracks(
            [in_p_2.d],
            tid_in_T_DS0,
            min_len_mode=1
        )
        warr_d_in_n2 = self.connect_to_tracks(
            [in_n_2.d],
            tid_in_T_DS1,
            min_len_mode=1
        )
        warr_d_diodL1 = self.connect_to_tracks(
            [diod_L_1.d],
            tid_load_B_DS,
            min_len_mode=1
        )
        warr_d_diodR1 = self.connect_to_tracks(
            [diod_R_1.d],
            tid_load_B_DS,
            min_len_mode=1
        )
        warr_d_ngmL1 = self.connect_to_tracks(
            [ngm_L_1.d],
            tid_load_B_DS,
            min_len_mode=1
        )
        warr_d_ngmR1 = self.connect_to_tracks(
            [ngm_R_1.d],
            tid_load_B_DS,
            min_len_mode=1
        )
        warr_d_diodL2 = self.connect_to_tracks(
            [diod_L_2.d],
            tid_load_T_DS,
            min_len_mode=1
        )
        warr_d_diodR2 = self.connect_to_tracks(
            [diod_R_2.d],
            tid_load_T_DS,
            min_len_mode=1
        )
        warr_d_ngmL2 = self.connect_to_tracks(
            [ngm_L_2.d],
            tid_load_T_DS,
            min_len_mode=1
        )
        warr_d_ngmR2 = self.connect_to_tracks(
            [ngm_R_2.d],
            tid_load_T_DS,
            min_len_mode=1
        )
        warr_g_diodL1 = self.connect_to_tracks(
            [diod_L_1.g, gain_L_1.g, comp_L_2.s, comp_L_1.s],
            tid_load_B_G,
            min_len_mode=1
        )
        warr_g_diodR1 = self.connect_to_tracks(
            [diod_R_1.g, gain_R_1.g, comp_R_2.s, comp_R_1.s],
            tid_load_B_G,
            min_len_mode=1
        )
        warr_g_ngmL1 = self.connect_to_tracks(
            [ngm_L_1.g],
            tid_load_B_G,
            min_len_mode=1
        )
        warr_g_ngmR1 = self.connect_to_tracks(
            [ngm_R_1.g],
            tid_load_B_G,
            min_len_mode=1
        )
        warr_g_diodL2 = self.connect_to_tracks(
            [diod_L_2.g],
            tid_load_T_G,
            min_len_mode=1
        )
        warr_g_diodR2 = self.connect_to_tracks(
            [diod_R_2.g],
            tid_load_T_G,
            min_len_mode=1
        )
        warr_g_ngmL2 = self.connect_to_tracks(
            [ngm_L_2.g],
            tid_load_T_G,
            min_len_mode=1
        )
        warr_g_ngmR2 = self.connect_to_tracks(
            [ngm_R_2.g],
            tid_load_T_G,
            min_len_mode=1
        )
        warr_g_in_p1 = self.connect_to_tracks(
            [in_p_1.g],
            tid_in_B_G0,
            min_len_mode=0,
        )
        warr_g_in_n1 = self.connect_to_tracks(
            [in_n_1.g],
            tid_in_B_G1,
            min_len_mode=0,
        )
        warr_g_in_p2 = self.connect_to_tracks(
            [in_p_2.g],
            tid_in_T_G0,
            min_len_mode=0,
        )
        warr_g_in_n2 = self.connect_to_tracks(
            [in_n_2.g],
            tid_in_T_G1,
            min_len_mode=0,
        )

        warr_g_tail1 = self.connect_to_tracks(
            [tail_1.g],
            tid_tail_B_G,
            min_len_mode=0,
        )
        warr_g_tail2 = self.connect_to_tracks(
            [tail_2.g, tail_L_2.g, tail_R_2.g, VB2B_R.g, VB2B_L.g, VBB.g, XRef.g, ENL.d, ENR.d],
            tid_tail_T_G,
            min_len_mode=0,
        )
        warr_d_tail_L_2 = self.connect_to_tracks(
            [tail_L_2.d],
            tid_tail_T_DS,
            min_len_mode=0,
        )
        warr_d_tail_R_2 = self.connect_to_tracks(
            [tail_R_2.d],
            tid_tail_T_DS,
            min_len_mode=0,
        )
        warr_d_gain_L1 = self.connect_to_tracks(
            [gain_L_1.d],
            tid_load_B_DSN,
            min_len_mode=0,
        )
        warr_d_gain_R1 = self.connect_to_tracks(
            [gain_R_1.d],
            tid_load_B_DSP,
            min_len_mode=0,
        )
        warr_d_gain_L2 = self.connect_to_tracks(
            [gain_L_2.d],
            tid_load_T_DS,
            min_len_mode=0,
        )
        warr_d_gain_R2 = self.connect_to_tracks(
            [gain_R_2.d],
            tid_load_T_DS,
            min_len_mode=0,
        )
        warr_g_gain_L2 = self.connect_to_tracks(
            [gain_L_2.g],
            tid_load_T_G,
            min_len_mode=0,
        )
        warr_g_gain_R2 = self.connect_to_tracks(
            [gain_R_2.g],
            tid_load_T_G,
            min_len_mode=0,
        )
        warr_g_EN = self.connect_to_tracks(
            [ENL.g, ENR.g],
            tid_tail_B_EN,
            min_len_mode=0,
        )
        warr_d_comp_L_1 = self.connect_to_tracks(
            [comp_L_1.d],
            tid_in_B_DS0,
            min_len_mode=0,
        )

        warr_d_comp_L_2 = self.connect_to_tracks(
            [comp_L_2.d],
            tid_in_T_DS0,
            min_len_mode=0,
        )
        warr_d_comp_R_2 = self.connect_to_tracks(
            [comp_R_2.d],
            tid_in_T_DS0,
            min_len_mode=0,
        )
        warr_d_cmr_L = self.connect_to_tracks(
            [cmr_L.d],
            tid_tail_B_DS,
            min_len_mode=0,
        )
        warr_d_cmr_R = self.connect_to_tracks(
            [cmr_R.d],
            tid_tail_B_DS,
            min_len_mode=0,
        )

        warr_g_st2_L1 = self.connect_to_tracks(
            [comp_L_1.g, VB2M_L_1.g],
            tid_in_B_G2,
            min_len_mode=0,
        )
        warr_g_st2_R1 = self.connect_to_tracks(
            [comp_R_1.g, VB2M_R_1.g],
            tid_in_B_G2,
            min_len_mode=0,
        )
        warr_g_st2_L2 = self.connect_to_tracks(
            [comp_L_2.g, VB2M_L_2.g, VB2M_L_2.d, VB2M_L_1.d, VB2M_L_1.g, VB2T_L.d],
            tid_in_T_G1,
            min_len_mode=0,
        )
        warr_g_st2_R2 = self.connect_to_tracks(
            [comp_R_2.g, VB2M_R_2.g, VB2M_R_2.d, VB2M_R_1.d, VB2M_R_1.g, VB2T_R.d],
            tid_in_T_G1,
            min_len_mode=0,
        )
        warr_g_st3_L = self.connect_to_tracks(
            [VB2B_L.d, VB2M_L_1.s, VB2T_L.g],
            tid_load_B_G,
            min_len_mode=0,
        )
        warr_g_st3_R = self.connect_to_tracks(
            [VB2B_R.d, VB2M_R_1.s, VB2T_R.g],
            tid_load_B_G,
            min_len_mode=0,
        )
        warr_g_cmfb = self.connect_to_tracks(
            [cmr_R.g, cmr_L.g],
            tid_tail_cm_G,
            min_len_mode=0,
        )
        warr_g_res = self.connect_to_tracks(
            [XRes.g, VBM.g],
            tid_in_B_G2,
            min_len_mode=0,
        )
        warr_d_ref = self.connect_to_tracks(
            [XRef.d, XRes.s],
            tid_tail_T_DS,
            min_len_mode=0,
        )
        warr_d_res = self.connect_to_tracks(
            [XRes.d],
            tid_in_B_DS1,
            min_len_mode=0,
        )
        warr_d_vbb = self.connect_to_tracks(
            [VBB.d, VBM.s],
            tid_tail_T_DS,
            min_len_mode=0,
        )
        warr_d_vbm = self.connect_to_tracks(
            [VBM.d],
            tid_in_B_DS1,
            min_len_mode=0,
        )
        warr_g_vbt = self.connect_to_tracks(
            [VBT.g, CMFB.g, VBT.d],
            tid_load_B_G,
            min_len_mode=0,
        )
        warr_d_cmfb = self.connect_to_tracks(
            [CMFB.d],
            tid_load_B_DS,
            min_len_mode=0,
        )

        # Perform the vertical connection
        self.connect_to_tracks(
            [warr_g_invn, warr_g_invp],
            tid_G_sideR_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_invn, warr_d_invp, warr_g_EN],
            tid_D_sideR_vert,
            min_len_mode=0,
        )

        self.connect_to_tracks(
            [warr_d_in_n1, warr_d_in_n2, warr_d_diodL1, warr_g_diodL1],
            tid_d1_L1_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_in_p2, warr_d_in_p1, warr_d_diodR2, warr_g_diodR2],
            tid_d2_L1_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_in_n1, warr_d_in_n2, warr_d_ngmL1, warr_g_ngmR2],
            tid_d1_L2_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_in_p2, warr_d_in_p1, warr_d_ngmR2, warr_g_ngmL1],
            tid_d2_L2_vert,
            min_len_mode=0,
        )

        self.connect_to_tracks(
            [warr_d_in_n2, warr_d_in_n1, warr_d_ngmL2, warr_g_ngmR1],
            tid_d1_R1_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_in_p1, warr_d_in_p2, warr_d_ngmR1, warr_g_ngmL2],
            tid_d2_R1_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_in_n2, warr_d_in_n1, warr_d_diodL2, warr_g_diodL2],
            tid_d1_R2_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_in_p1, warr_d_in_p2, warr_d_diodR1, warr_g_diodR1],
            tid_d2_R2_vert,
            min_len_mode=0,
        )

        self.connect_to_tracks(
            [warr_g_in_p1, warr_g_in_p2],
            tid_G1_L_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_g_in_n1, warr_g_in_n2],
            tid_G2_L_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_g_in_p1, warr_g_in_p2],
            tid_G1_R_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_g_in_n1, warr_g_in_n2],
            tid_G2_R_vert,
            min_len_mode=0,
        )

        self.connect_to_tracks(
            [warr_g_tail1, warr_g_tail2],
            tid_G_tail_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_tail1, warr_d_tail2, warr_s_in],
            tid_d_tail_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_tail_L_2, warr_d_gain_L1, warr_d_gain_L2, warr_d_cmr_L],
            tid_d_st2_L_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_tail_R_2, warr_d_gain_R1, warr_d_gain_R2, warr_d_cmr_R],
            tid_d_st2_R_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_g_vbt, warr_d_vbm],
            tid_d_sid2_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_g_tail2, warr_d_res],
            tid_d_side1_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_comp_R_1, warr_d_comp_R_2],
            tid_d_st2_R1_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_d_comp_L_1, warr_d_comp_L_2],
            tid_d_st2_L1_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_g_diodL1, warr_g_gain_L2],
            tid_d_st2_L1_vert,
            min_len_mode=0,
        )
        self.connect_to_tracks(
            [warr_g_diodR1, warr_g_gain_R2],
            tid_d_st2_R1_vert,
            min_len_mode=0,
        )

        # ################################################################################
        # # 8:  Connections to substrate, and dummy fill
        # #
        # # - Use the self.connect_to_substrate method to perform wiring to the ntap and ptap where VDD and VSS will be
        # # - Use self.fill_dummy() draw the dummy transistor structures, and finalize the VDD and VSS wiring
        # #   - This method should be called last!
        # ################################################################################
        # Connections to VSS
        self.connect_to_substrate(
            'ptap',
            [tail_1.s, tail_2.s, tail_L_2.s, tail_R_2.s,
             cmr_L.s, cmr_L.s, VB2B_L.s, VB2B_R.s,
             VBB.s, XRef.s, ENL.s, ENR.s, Invn.s]
        )

        # Connections to VDD
        self.connect_to_substrate(
            'ntap',
            [ngm_L_1.s, ngm_L_2.s, ngm_R_1.s, ngm_R_2.s,
             diod_L_1.s, diod_L_2.s, diod_R_1.s, diod_R_2.s,
             gain_L_1.s, gain_L_2.s, gain_R_1.s, gain_R_2.s,
             VB2T_L.s, VB2T_R.s, VBT.s, CMFB.s, Invp.s]
        )
        warr_vss, warr_vdd = self.fill_dummy()

        # ################################################################################
        # # 9:  Add pins
        # #
        # ################################################################################
        self.add_pin('VDD', warr_vdd, show=params.show_pins)

        self.add_pin('VSS', warr_vss, show=params.show_pins)
        self.add_pin('inn', warr_g_in_n1, show=params.show_pins)
        self.add_pin('inp', warr_g_in_p1, show=params.show_pins)
        self.add_pin('cmfb_ibias', warr_d_cmfb, show=params.show_pins)
        self.add_pin('bias', warr_d_res, show=params.show_pins)
        self.add_pin('ref_in', warr_g_res, show=params.show_pins)
        self.add_pin('en_amp', warr_g_invn, show=params.show_pins)
        self.add_pin('outn', warr_d_gain_L1, show=params.show_pins)
        self.add_pin('outp', warr_d_gain_R1, show=params.show_pins)
        self.add_pin('cmbias', warr_g_cmfb, show=params.show_pins)
        self.add_pin('midn', warr_g_diodR1, show=params.show_pins)
        self.add_pin('midp', warr_g_diodL1, show=params.show_pins)
        self.add_pin('xn', warr_d_comp_L_1, show=params.show_pins)
        self.add_pin('xp', warr_d_comp_R_1, show=params.show_pins)

        ################################################################################
        # 10:  Organize parameters for the schematic generator
        #
        # To make the schematic generator very simple, organize the required transistor information (fins/widith,
        # intent, and number of fingers) into a convenient data structure
        # Finally set the self._sch_params property so that these parameters are accessible to the schematic generator
        ################################################################################
        # Define transistor properties for schematic
        tx_info = {}
        for tx in transistors:
            tx_info[tx.name] = {
                'w': tx.row.width,
                'th': tx.row.threshold,
                'fg': tx.fg
            }

        self._sch_params = dict(
            lch=params.lch,
            dum_info=self.get_sch_dummy_info(),
            tx_info=tx_info,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class two_stage_opamp_nmos(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
