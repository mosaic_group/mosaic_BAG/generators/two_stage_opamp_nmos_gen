#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *
from sal.testbench_base import TestbenchBase
from sal.testbench_params import *


@dataclass
class two_stage_opamp_nmos_layout_params(LayoutParamsBase):
    """
    Parameter class for two_stage_opamp_nmos_gen

    Args:
    ----
    ntap_w : Union[float, int]
        Width of the N substrate contact

    ptap_w : Union[float, int]
        Width of P substrate contact

    lch : float
        Channel length of the transistors

    lch_dict : Dict[str, float]
        Channel length of the transistors dictionary

    th_dict : Dict[str, str]
        NMOS/PMOS threshold flavor dictionary

    fg_dum : int
        Number of fingers in NMOS transistor

    tr_spaces : Dict[str, int]
        Track spacing dictionary

    tr_widths : Dict[str, Dict[int, int]]
        Track width dictionary

    top_layer: int
        Top metal Layer used in Layout

    ndum_mid_stages : int
        number of dummy fingers/transistors between stages/columns of transistors

    w_dict : Dict[str, Union[float, int]]
        NMOS/PMOS width dictionary

    stack_dict : Dict[str, int]
        Stack dictionary

    seg_dict : Dict[str, int]
        NMOS/PMOS number of segments dictionary

    guard_ring_nf : int
        Width of guard ring

    show_pins : bool
        True to create pin labels
    """

    ntap_w: Union[float, int]
    ptap_w: Union[float, int]
    lch: float
    lch_dict: Dict[str, float]
    th_dict: Dict[str, str]
    fg_dum: int
    tr_spaces: Dict[str, int]
    tr_widths: Dict[str, Dict[int, int]]
    top_layer: int
    w_dict: Dict[str, Union[float, int]]
    stack_dict: Dict[str, int]
    seg_dict: Dict[str, int]
    guard_ring_nf: int
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> two_stage_opamp_nmos_layout_params:
        return two_stage_opamp_nmos_layout_params(
            ntap_w=10,
            ptap_w=10,
            lch=min_lch,
            lch_dict={
               'diode': min_lch,
               'in': min_lch,
               'ngm': min_lch,
               'tail': min_lch,
               'load': min_lch
            },
            th_dict={
               'Dummy_NB': 'standard',
               'tail_B': 'standard',
               'tail_T': 'standard',
               'In_B': 'standard',
               'In_T': 'standard',
               'Dummy_NT': 'standard',
               'Dummy_PB': 'standard',
               'load_B': 'standard',
               'load_T': 'standard',
               'Dummy_PT': 'standard',
            },
            fg_dum=4,
            tr_spaces={},
            tr_widths={'sig': {4: 1}},
            top_layer=5,
            w_dict={
                'Dummy_NB': 10,
                'tail_B': 10,
                'tail_T': 10,
                'In_B': 10,
                'In_T': 10,
                'Dummy_NT': 10,
                'Dummy_PB': 10,
                'load_B': 10,
                'load_T': 10,
                'Dummy_PT': 10,
            },
            stack_dict={
                'diode': 1,
                'in': 1,
                'ngm': 1,
                'tail': 1,
            },
            seg_dict={
                'diode1': 8,  # 8 more than 4
                'diode2': 12,  # 12
                'in': 24,  # 24
                'ngm1': 6,  # 6 more than 4
                'ngm2': 12,  # 12
                'ref': 4,
                'res': 4,
                'tail1': 40,  # 40
                'tail2': 6,
                'tailcm': 2,
                'vbb': 4,
                'vb2b': 4,
                'vbm': 4,
                'vb2m': 8,  # 8
                'vbt': 2,
                'vb2t': 2,
                'cmfb': 2,
                'comp': 16,  # 16
                'en': 6,
                'Invn': 2,
                'Invp': 6,
            },
            guard_ring_nf=0,
            show_pins=True,
            )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> two_stage_opamp_nmos_layout_params:
        return two_stage_opamp_nmos_layout_params(
            ntap_w=10 * min_lch,
            ptap_w=10 * min_lch,
            lch=min_lch*3,
            lch_dict={
               'diode': min_lch,
               'in': min_lch,
               'ngm': min_lch,
               'tail': min_lch,
               'load': min_lch
            },
            th_dict={
               'Dummy_NB': 'lvt',
               'tail_B': 'lvt',
               'tail_T': 'lvt',
               'In_B': 'lvt',
               'In_T': 'lvt',
               'Dummy_NT': 'lvt',
               'Dummy_PB': 'lvt',
               'load_B': 'lvt',
               'load_T': 'lvt',
               'Dummy_PT': 'lvt',
            },
            fg_dum=4,
            tr_spaces={},
            tr_widths={'sig': {4: 1}},
            top_layer=5,
            w_dict={
                'Dummy_NB': 10 * min_lch,
                'tail_B': 10 * min_lch,
                'tail_T': 10 * min_lch,
                'In_B': 10 * min_lch,
                'In_T': 10 * min_lch,
                'Dummy_NT': 10 * min_lch,
                'Dummy_PB': 10 * min_lch,
                'load_B': 10 * min_lch,
                'load_T': 10 * min_lch,
                'Dummy_PT': 10 * min_lch,
            },
            stack_dict={
                'diode': 1,
                'in': 1,
                'ngm': 1,
                'tail': 1,
            },
            seg_dict={
                'diode1': 8,  # 8 more than 4
                'diode2': 12,  # 12
                'in': 24,  # 24
                'ngm1': 6,  # 6 more than 4
                'ngm2': 12,  # 12
                'ref': 4,
                'res': 4,
                'tail1': 40,  # 40
                'tail2': 6,
                'tailcm': 2,
                'vbb': 4,
                'vb2b': 4,
                'vbm': 4,
                'vb2m': 8,  # 8
                'vbt': 2,
                'vb2t': 2,
                'cmfb': 2,
                'comp': 16,  # 16
                'en': 6,
                'Invn': 2,
                'Invp': 6,
            },
            guard_ring_nf=2,
            show_pins=True,
        )


@dataclass
class two_stage_opamp_nmos_params(GeneratorParamsBase):
    layout_parameters: two_stage_opamp_nmos_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> tgate_params:
        return two_stage_opamp_nmos_params(
            layout_parameters=two_stage_opamp_nmos_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
