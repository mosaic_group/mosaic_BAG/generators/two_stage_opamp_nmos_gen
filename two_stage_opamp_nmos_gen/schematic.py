#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from typing import *
from bag.design import Module

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/two_stage_opamp_nmos_templates',
                         'netlist_info', 'two_stage_opamp_nmos.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library two_stage_opamp_nmos_templates cell two_stage_opamp_nmos.

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        super().__init__(bag_config, yaml_file, parent=parent, prj=prj, **kwargs)
       
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            lch='inverter/switch channel length, in meters.',
            tx_info='List of dictionaries of transistor information',
            dum_info='Dummy information data structure.', 
        )

    def design(self,
               lch: float,
               tx_info: Dict[str, Dict[str, Any]],
               dum_info: List[Tuple[Any]]):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """

        tran_info_list1 = [
            ('XTAILL', 'tail_1'), ('XTAILR', 'tail_2'),
            ('XDIO2L', 'gain_L_1'), ('XDIO2R', 'gain_R_1'),
            ('XNGM2L', 'gain_L_2'), ('XNGM2R', 'gain_R_2'),
            ('XTAIL2L', 'tail_L_2'), ('XTAIL2R', 'tail_R_2'),
            ('XCML', 'cmr_L'), ('XCMR', 'cmr_R'),
            ('XVB2BL', 'VB2B_L'), ('XVB2BR', 'VB2B_R'),
            ('XVB2TL', 'VB2T_L'), ('XVB2TR', 'VB2T_R'),
            ('XVBB', 'VBB'), ('XRES', 'XRes'),
            ('XVBM', 'VBM'), ('XREF', 'XRef'),
            ('XVBT', 'VBT'), ('XCMFB', 'CMFB'),
            ('XENL', 'ENL'), ('XENR', 'ENR'),
            ('Invn', 'Invn'), ('Invp', 'Invp'),
        ]
        tran_info_list2 = [
            ('XINL', 'in_n_1'), ('XINR', 'in_p_1'),
            ('XNGML', 'ngm_L_1'), ('XNGMR', 'ngm_R_1'),
            ('XDIOL', 'diod_L_1'), ('XDIOR', 'diod_R_1'),
            ('XCOMPL', 'comp_L_1'), ('XCOMPR', 'comp_R_1'),
            ('XVB2ML', 'VB2M_L_1'), ('XVB2MR', 'VB2M_R_1'),
        ]

        for sch_name, layout_name in tran_info_list1:
            w = tx_info[layout_name]['w']
            th = tx_info[layout_name]['th']
            nf = tx_info[layout_name]['fg']
            self.instances[sch_name].design(w=w, l=lch, nf=nf, intent=th, multi=1)

        for sch_name, layout_name in tran_info_list2:
            w = tx_info[layout_name]['w']
            th = tx_info[layout_name]['th']
            nf = tx_info[layout_name]['fg']
            self.instances[sch_name].design(w=w, l=lch, nf=2*nf, intent=th, multi=2)

        # design dummies
        self.design_dummy_transistors(dum_info, 'XDUM', 'VDD', 'VSS')
