v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 450 -640 450 -590 {
lab=VDD}
N 450 -640 610 -640 {
lab=VDD}
N 610 -640 610 -590 {
lab=VDD}
N 380 -560 450 -560 {
lab=VDD}
N 380 -620 380 -560 {
lab=VDD}
N 380 -620 450 -620 {
lab=VDD}
N 490 -560 570 -560 {
lab=vbm}
N 450 -530 450 -500 {
lab=cmfb_ibias}
N 610 -530 610 -500 {
lab=vbm}
N 560 -500 610 -500 {
lab=vbm}
N 560 -560 560 -500 {
lab=vbm}
N 760 -560 780 -560 {
lab=VDD}
N 760 -590 760 -560 {
lab=VDD}
N 760 -590 780 -590 {
lab=VDD}
N 610 -640 780 -640 {
lab=VDD}
N 780 -640 780 -590 {
lab=VDD}
N 780 -530 780 -490 {
lab=vxn}
N 780 -490 780 -470 {
lab=vxn}
N 820 -490 820 -440 {
lab=vxn}
N 780 -490 820 -490 {
lab=vxn}
N 780 -410 780 -360 {
lab=vb2tn}
N 820 -560 860 -560 {
lab=vb2tn}
N 860 -560 880 -560 {
lab=vb2tn}
N 880 -560 880 -380 {
lab=vb2tn}
N 780 -380 880 -380 {
lab=vb2tn}
N 1020 -560 1050 -560 {
lab=VDD}
N 1020 -590 1020 -560 {
lab=VDD}
N 1020 -590 1050 -590 {
lab=VDD}
N 780 -640 1050 -640 {
lab=VDD}
N 1050 -640 1050 -590 {
lab=VDD}
N 1170 -560 1190 -560 {
lab=VDD}
N 1170 -590 1170 -560 {
lab=VDD}
N 1170 -590 1190 -590 {
lab=VDD}
N 1050 -640 1190 -640 {
lab=VDD}
N 1190 -640 1190 -590 {
lab=VDD}
N 1090 -610 1090 -560 {
lab=midp}
N 1090 -610 1230 -610 {
lab=midp}
N 1230 -610 1230 -560 {
lab=midp}
N 1050 -530 1050 -510 {
lab=outn}
N 1050 -510 1190 -510 {
lab=outn}
N 1190 -530 1190 -510 {
lab=outn}
N 1190 -510 1190 -470 {
lab=outn}
N 1190 -640 1300 -640 {
lab=VDD}
N 1230 -560 1350 -560 {
lab=midp}
N 1390 -560 1410 -560 {
lab=VDD}
N 1410 -590 1410 -560 {
lab=VDD}
N 1390 -590 1410 -590 {
lab=VDD}
N 1300 -640 1390 -640 {
lab=VDD}
N 1390 -640 1390 -590 {
lab=VDD}
N 1390 -530 1390 -500 {
lab=midp}
N 1510 -560 1530 -560 {
lab=VDD}
N 1510 -590 1510 -560 {
lab=VDD}
N 1510 -590 1530 -590 {
lab=VDD}
N 1390 -500 1530 -500 {
lab=midp}
N 1530 -530 1530 -500 {
lab=midp}
N 1390 -640 1530 -640 {
lab=VDD}
N 1530 -640 1530 -590 {
lab=VDD}
N 1470 -500 1470 -440 {
lab=midp}
N 1870 -560 1890 -560 {
lab=VDD}
N 1890 -590 1890 -560 {
lab=VDD}
N 1870 -590 1890 -590 {
lab=VDD}
N 1870 -640 1870 -590 {
lab=VDD}
N 1870 -530 1870 -500 {
lab=midn}
N 1990 -560 2010 -560 {
lab=VDD}
N 1990 -590 1990 -560 {
lab=VDD}
N 1990 -590 2010 -590 {
lab=VDD}
N 1870 -500 2010 -500 {
lab=midn}
N 2010 -530 2010 -500 {
lab=midn}
N 1870 -640 2010 -640 {
lab=VDD}
N 2010 -640 2010 -590 {
lab=VDD}
N 1470 -470 1770 -470 {
lab=midp}
N 1770 -560 1770 -470 {
lab=midp}
N 1770 -560 1830 -560 {
lab=midp}
N 2010 -500 2050 -500 {
lab=midn}
N 2050 -560 2050 -500 {
lab=midn}
N 1530 -640 1870 -640 {
lab=VDD}
N 1350 -560 1350 -500 {
lab=midp}
N 1350 -500 1390 -500 {
lab=midp}
N 1870 -500 1870 -430 {
lab=midn}
N 1730 -480 1870 -480 {
lab=midn}
N 1650 -480 1730 -480 {
lab=midn}
N 1570 -560 1620 -560 {
lab=midn}
N 1620 -560 1620 -480 {
lab=midn}
N 1620 -480 1650 -480 {
lab=midn}
N 2010 -640 2390 -640 {
lab=VDD}
N 2390 -640 2390 -590 {
lab=VDD}
N 2230 -640 2230 -590 {
lab=VDD}
N 2350 -610 2350 -560 {
lab=midn}
N 2190 -610 2350 -610 {
lab=midn}
N 2190 -610 2190 -560 {
lab=midn}
N 2230 -590 2250 -590 {
lab=VDD}
N 2250 -590 2250 -560 {
lab=VDD}
N 2230 -560 2250 -560 {
lab=VDD}
N 2390 -560 2410 -560 {
lab=VDD}
N 2410 -590 2410 -560 {
lab=VDD}
N 2390 -590 2410 -590 {
lab=VDD}
N 2230 -530 2230 -490 {
lab=outp}
N 2230 -490 2390 -490 {
lab=outp}
N 2390 -530 2390 -490 {
lab=outp}
N 2230 -490 2230 -240 {
lab=outp}
N 2390 -640 2560 -640 {
lab=VDD}
N 2560 -640 2560 -590 {
lab=VDD}
N 2560 -590 2580 -590 {
lab=VDD}
N 2580 -590 2580 -560 {
lab=VDD}
N 2560 -560 2580 -560 {
lab=VDD}
N 2560 -530 2560 -490 {
lab=vxp}
N 2520 -510 2560 -510 {
lab=vxp}
N 2520 -510 2520 -460 {
lab=vxp}
N 2560 -430 2560 -400 {
lab=vb2tp}
N 2480 -560 2520 -560 {
lab=vb2tp}
N 2480 -560 2480 -400 {
lab=vb2tp}
N 2480 -400 2560 -400 {
lab=vb2tp}
N 2560 -400 2560 -360 {
lab=vb2tp}
N 2840 -510 2860 -510 {
lab=VDD}
N 2860 -540 2860 -510 {
lab=VDD}
N 2840 -540 2860 -540 {
lab=VDD}
N 2560 -640 2730 -640 {
lab=VDD}
N 2840 -480 2840 -440 {
lab=ENB}
N 2780 -510 2800 -510 {
lab=en_amp}
N 2780 -510 2780 -410 {
lab=en_amp}
N 2780 -410 2800 -410 {
lab=en_amp}
N 2760 -460 2780 -460 {
lab=en_amp}
N 2840 -640 2840 -540 {
lab=VDD}
N 2730 -640 2840 -640 {
lab=VDD}
N 2840 -410 2870 -410 {
lab=VSS}
N 2870 -410 2870 -380 {
lab=VSS}
N 2840 -380 2870 -380 {
lab=VSS}
N 2840 -460 2890 -460 {
lab=ENB}
N 2840 -380 2840 -340 {
lab=VSS}
N 3070 -350 3070 -330 {
lab=VSS}
N 3010 -300 3030 -300 {
lab=VSS}
N 3070 -270 3070 -250 {
lab=VSS}
N 3070 -300 3110 -300 {
lab=VSS}
N 2150 -370 2170 -370 {
lab=vxp}
N 2110 -430 2110 -400 {
lab=midn}
N 2080 -370 2110 -370 {
lab=VSS}
N 2110 -340 2110 -320 {
lab=xp}
N 2560 -460 2570 -460 {
lab=VSS}
N 2570 -460 2600 -460 {
lab=VSS}
N 2560 -360 2560 -290 {
lab=vb2tp}
N 2560 -120 2560 -90 {
lab=VSS}
N 2560 -90 2840 -90 {
lab=VSS}
N 2840 -340 2840 -200 {
lab=VSS}
N 2560 -120 2580 -120 {
lab=VSS}
N 2580 -150 2580 -120 {
lab=VSS}
N 2560 -150 2580 -150 {
lab=VSS}
N 2840 -200 2840 -90 {
lab=VSS}
N 2560 -290 2560 -180 {
lab=vb2tp}
N 2420 -150 2440 -150 {
lab=VSS}
N 2440 -150 2440 -120 {
lab=VSS}
N 2420 -120 2440 -120 {
lab=VSS}
N 2420 -120 2420 -90 {
lab=VSS}
N 2420 -90 2560 -90 {
lab=VSS}
N 2520 -190 2520 -150 {
lab=bias}
N 2420 -190 2520 -190 {
lab=bias}
N 2420 -190 2420 -180 {
lab=bias}
N 2350 -150 2380 -150 {
lab=ENB}
N 2400 -190 2420 -190 {
lab=bias}
N 2150 -210 2150 -180 {
lab=outp}
N 2150 -210 2260 -210 {
lab=outp}
N 2260 -210 2260 -180 {
lab=outp}
N 2230 -240 2230 -210 {
lab=outp}
N 2300 -150 2320 -150 {
lab=cmbias}
N 2150 -90 2420 -90 {
lab=VSS}
N 2150 -120 2150 -90 {
lab=VSS}
N 2260 -120 2260 -90 {
lab=VSS}
N 2060 -190 2400 -190 {
lab=bias}
N 2110 -190 2110 -150 {
lab=bias}
N 2250 -150 2260 -150 {
lab=VSS}
N 2250 -150 2250 -120 {
lab=VSS}
N 2250 -120 2260 -120 {
lab=VSS}
N 2150 -150 2160 -150 {
lab=VSS}
N 2160 -150 2160 -120 {
lab=VSS}
N 2150 -120 2160 -120 {
lab=VSS}
N 1870 -430 1870 -340 {
lab=midn}
N 1910 -310 1930 -310 {
lab=inp}
N 1830 -310 1870 -310 {
lab=VSS}
N 1870 -280 1870 -260 {
lab=tail}
N 1870 -260 1870 -180 {
lab=tail}
N 1910 -190 2060 -190 {
lab=bias}
N 1910 -190 1910 -150 {
lab=bias}
N 1910 -190 1910 -150 {
lab=bias}
N 1760 -190 1910 -190 {
lab=bias}
N 1850 -150 1870 -150 {
lab=VSS}
N 1850 -150 1850 -120 {
lab=VSS}
N 1850 -120 1870 -120 {
lab=VSS}
N 1870 -120 1870 -100 {
lab=VSS}
N 1870 -100 1870 -90 {
lab=VSS}
N 1870 -90 2150 -90 {
lab=VSS}
N 1470 -440 1470 -340 {
lab=midp}
N 1410 -310 1430 -310 {
lab=inn}
N 1470 -310 1510 -310 {
lab=VSS}
N 1470 -280 1470 -240 {
lab=tail}
N 1470 -240 1870 -240 {
lab=tail}
N 1470 -120 1480 -120 {
lab=VSS}
N 1480 -150 1480 -120 {
lab=VSS}
N 1470 -150 1480 -150 {
lab=VSS}
N 1470 -120 1470 -90 {
lab=VSS}
N 1470 -90 1870 -90 {
lab=VSS}
N 1360 -190 1760 -190 {
lab=bias}
N 1360 -190 1360 -150 {
lab=bias}
N 1360 -150 1430 -150 {
lab=bias}
N 1260 -150 1360 -150 {
lab=bias}
N 1260 -190 1360 -190 {
lab=bias}
N 1190 -470 1190 -320 {
lab=outn}
N 1190 -320 1190 -280 {
lab=outn}
N 1320 -420 1320 -400 {
lab=midp}
N 1260 -370 1280 -370 {
lab=vxn}
N 1320 -340 1320 -320 {
lab=xn}
N 1320 -370 1350 -370 {
lab=VSS}
N 1060 -180 1190 -180 {
lab=outn}
N 1190 -280 1190 -180 {
lab=outn}
N 1230 -150 1260 -150 {
lab=bias}
N 970 -190 1260 -190 {
lab=bias}
N 990 -150 1020 -150 {
lab=cmbias}
N 1060 -90 1470 -90 {
lab=VSS}
N 1060 -120 1060 -90 {
lab=VSS}
N 1190 -120 1190 -90 {
lab=VSS}
N 1060 -150 1070 -150 {
lab=VSS}
N 1070 -150 1070 -120 {
lab=VSS}
N 1060 -120 1070 -120 {
lab=VSS}
N 1180 -150 1190 -150 {
lab=VSS}
N 1180 -150 1180 -120 {
lab=VSS}
N 1180 -120 1190 -120 {
lab=VSS}
N 900 -150 910 -150 {
lab=VSS}
N 900 -150 900 -120 {
lab=VSS}
N 900 -120 910 -120 {
lab=VSS}
N 930 -150 950 -150 {
lab=ENB}
N 910 -190 910 -180 {
lab=bias}
N 890 -190 970 -190 {
lab=bias}
N 710 -90 1060 -90 {
lab=VSS}
N 780 -360 780 -170 {
lab=vb2tn}
N 910 -120 910 -90 {
lab=VSS}
N 820 -190 890 -190 {
lab=bias}
N 710 -190 820 -190 {
lab=bias}
N 780 -110 780 -90 {
lab=VSS}
N 820 -190 820 -150 {
lab=bias}
N 770 -150 780 -150 {
lab=VSS}
N 770 -120 780 -120 {
lab=VSS}
N 780 -120 780 -110 {
lab=VSS}
N 770 -150 770 -120 {
lab=VSS}
N 610 -500 610 -300 {
lab=vbm}
N 610 -270 660 -270 {
lab=VSS}
N 550 -270 570 -270 {
lab=ref_in}
N 610 -240 610 -180 {
lab=vbb}
N 610 -120 610 -90 {
lab=VSS}
N 610 -90 710 -90 {
lab=VSS}
N 550 -190 710 -190 {
lab=bias}
N 550 -190 550 -150 {
lab=bias}
N 550 -150 570 -150 {
lab=bias}
N 370 -270 420 -270 {
lab=VSS}
N 370 -340 370 -300 {
lab=bias}
N 370 -150 380 -150 {
lab=VSS}
N 380 -150 380 -120 {
lab=VSS}
N 370 -120 380 -120 {
lab=VSS}
N 370 -90 610 -90 {
lab=VSS}
N 370 -240 370 -200 {
lab=ref}
N 370 -200 370 -180 {
lab=ref}
N 370 -120 370 -90 {
lab=VSS}
N 310 -310 370 -310 {
lab=bias}
N 310 -190 550 -190 {
lab=bias}
N 290 -190 310 -190 {
lab=bias}
N 290 -190 290 -150 {
lab=bias}
N 290 -150 330 -150 {
lab=bias}
N 610 -150 620 -150 {
lab=VSS}
N 620 -150 620 -120 {
lab=VSS}
N 610 -120 620 -120 {
lab=VSS}
N 1470 -240 1470 -180 {
lab=tail}
N 310 -310 310 -190 {
lab=bias}
N 280 -270 330 -270 {
lab=ref_in}
N 2050 -560 2190 -560 {
lab=midn}
N 740 -440 780 -440 {
lab=VSS}
N 610 -560 630 -560 {
lab=VDD}
N 630 -590 630 -560 {
lab=VDD}
N 610 -590 630 -590 {
lab=VDD}
C {devices/iopin.sym} 170 -820 2 0 {name=p1 lab=VDD}
C {devices/iopin.sym} 170 -800 2 0 {name=p2 lab=VSS}
C {devices/iopin.sym} 170 -780 2 0 {name=p3 lab=inp}
C {devices/iopin.sym} 170 -760 2 0 {name=p4 lab=inn}
C {devices/iopin.sym} 170 -740 2 0 {name=p5 lab=cmbias}
C {devices/iopin.sym} 170 -720 2 0 {name=p6 lab=bias}
C {devices/iopin.sym} 170 -700 2 0 {name=p7 lab=ref_in}
C {devices/iopin.sym} 170 -680 2 0 {name=p8 lab=en_amp}
C {devices/iopin.sym} 310 -820 2 0 {name=p9 lab=outp}
C {devices/iopin.sym} 310 -800 2 0 {name=p10 lab=outn}
C {devices/iopin.sym} 310 -780 2 0 {name=p11 lab=midp}
C {devices/iopin.sym} 310 -760 2 0 {name=p12 lab=midn}
C {devices/iopin.sym} 310 -720 2 0 {name=p13 lab=cmfb_ibias}
C {devices/iopin.sym} 310 -700 2 0 {name=p14 lab=xp}
C {devices/iopin.sym} 310 -680 2 0 {name=p15 lab=xn}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 470 -560 0 1 {name=XCMFB
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 590 -560 0 0 {name=XVBT
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 450 -500 0 0 {name=l1 sig_type=std_logic lab=cmfb_ibias}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 800 -560 0 1 {name=XVB2TL
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 800 -440 0 1 {name=XVB2ML
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 820 -480 2 0 {name=l2 sig_type=std_logic lab=vxn}
C {devices/lab_pin.sym} 880 -430 2 0 {name=l3 sig_type=std_logic lab=vb2tn}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 1070 -560 0 1 {name=XDIO2L
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 1210 -560 0 1 {name=XNGM2L
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 1240 -640 1 0 {name=l4 sig_type=std_logic lab=VDD}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 1370 -560 0 0 {name=XDIOL
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 1550 -560 0 1 {name=XNGML
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 1470 -450 0 0 {name=l5 sig_type=std_logic lab=midp}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 2030 -560 0 1 {name=XDIOR
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 1850 -560 0 0 {name=XNGMR
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 1870 -450 2 0 {name=l6 sig_type=std_logic lab=midn}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 2370 -560 0 0 {name=XDIO2R
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 2210 -560 0 0 {name=XNGM2R
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 2230 -340 2 0 {name=l7 sig_type=std_logic lab=outp}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 2540 -560 0 0 {name=XVB2TR
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 2540 -460 0 0 {name=XVB2MR
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 2520 -510 0 0 {name=l8 sig_type=std_logic lab=vxp}
C {devices/lab_pin.sym} 2480 -420 0 0 {name=l9 sig_type=std_logic lab=vb2tp}
C {BAG_prim/pmos4_lvt/pmos4_lvt.sym} 2820 -510 0 0 {name=Invp
w=320n
l=90n
nf=4
model=pmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 2820 -410 0 0 {name=Invn
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 2760 -460 0 0 {name=l10 sig_type=std_logic lab=en_amp}
C {devices/lab_pin.sym} 2890 -460 2 0 {name=l11 sig_type=std_logic lab=ENB}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 3050 -300 0 0 {name=XDUM
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 3070 -350 0 0 {name=l12 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 3010 -300 0 0 {name=l13 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 3110 -300 2 0 {name=l14 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 3070 -250 2 0 {name=l15 sig_type=std_logic lab=VSS}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 2130 -370 2 0 {name=XCOMPR
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 2170 -370 2 0 {name=l16 sig_type=std_logic lab=vxp}
C {devices/lab_pin.sym} 2110 -320 2 0 {name=l17 sig_type=std_logic lab=xp}
C {devices/lab_pin.sym} 2110 -430 2 0 {name=l18 sig_type=std_logic lab=midn}
C {devices/lab_pin.sym} 2080 -370 0 0 {name=l19 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 2600 -460 2 0 {name=l20 sig_type=std_logic lab=VSS}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 2540 -150 0 0 {name=XVB2BR
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 2400 -150 0 0 {name=XENR
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 2360 -150 1 0 {name=l21 sig_type=std_logic lab=ENB}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 2130 -150 0 0 {name=XTAIL2R
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 2280 -150 0 1 {name=XCMR
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 2320 -150 3 0 {name=l22 sig_type=std_logic lab=cmbias}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 1890 -310 0 1 {name=XINR
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 1830 -310 0 0 {name=l23 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1930 -310 2 0 {name=l24 sig_type=std_logic lab=inp}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 1890 -150 0 1 {name=XTAILR
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 1450 -310 0 0 {name=XINL
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 1510 -310 2 0 {name=l25 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 1410 -310 0 0 {name=l26 sig_type=std_logic lab=inn}
C {devices/lab_pin.sym} 1640 -240 1 0 {name=l27 sig_type=std_logic lab=tail}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 1450 -150 0 0 {name=XTAILL
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 1190 -380 0 0 {name=l28 sig_type=std_logic lab=outn}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 1300 -370 2 1 {name=XCOMPL
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 1320 -420 0 0 {name=l29 sig_type=std_logic lab=midp}
C {devices/lab_pin.sym} 1260 -370 0 0 {name=l30 sig_type=std_logic lab=vxn}
C {devices/lab_pin.sym} 1320 -320 2 0 {name=l31 sig_type=std_logic lab=xn}
C {devices/lab_pin.sym} 1350 -370 2 0 {name=l32 sig_type=std_logic lab=VSS}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 1210 -150 0 1 {name=XTAIL2L
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 1040 -150 0 0 {name=XCML
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 990 -150 3 0 {name=l33 sig_type=std_logic lab=cmbias}
C {devices/lab_pin.sym} 1520 -90 3 0 {name=l34 sig_type=std_logic lab=VSS}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 930 -150 0 1 {name=XENL
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 950 -150 3 0 {name=l35 sig_type=std_logic lab=ENB}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 800 -150 0 1 {name=XVB2BL
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 590 -270 0 0 {name=XVBM
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 610 -390 2 0 {name=l36 sig_type=std_logic lab=vbm}
C {devices/lab_pin.sym} 660 -270 2 0 {name=l37 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 550 -270 0 0 {name=l38 sig_type=std_logic lab=ref_in}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 590 -150 0 0 {name=XVBB
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 350 -270 0 0 {name=XRES
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 420 -270 2 0 {name=l39 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 280 -270 0 0 {name=l40 sig_type=std_logic lab=ref_in}
C {devices/lab_pin.sym} 370 -340 2 0 {name=l41 sig_type=std_logic lab=bias}
C {BAG_prim/nmos4_lvt/nmos4_lvt.sym} 350 -150 0 0 {name=XREF
w=500n
l=90n
nf=6
model=nmos4_lvt
spiceprefix=X
}
C {devices/lab_pin.sym} 370 -220 0 0 {name=l42 sig_type=std_logic lab=ref}
C {devices/lab_pin.sym} 740 -440 0 0 {name=l43 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 610 -220 0 0 {name=l44 sig_type=std_logic lab=vbb}
